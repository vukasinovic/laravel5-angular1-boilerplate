<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Validator;

class AuthController extends Controller
{
    public function authenticate(Request $request) {

        $payload = $request->only('email', 'password');
        $rules = array(
            'email' => 'required',
            'password' => 'required'
        );
        $validator = Validator::make($payload, $rules);

        if($validator->fails()) {
            return response()->json(array(
                'errors' => true,
                'message' => $validator->errors()
            ));
        }

        $token = JWTAuth::attempt($payload);

        if(!$token) {
            return response()->json(array(
                'errors' => true,
                'message' => 'Login attempt failed.'
            ));
        }

        return response()->json(array(
            'errors' => false,
            'token' => $token
        ));

    }
}
